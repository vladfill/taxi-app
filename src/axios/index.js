import axios from "axios"
import {REQUEST_URI} from '../config'

export default (token='') => {
  if( token ) {
    return axios.create({
      baseURL: `${REQUEST_URI}/`,
      timeout: 1000,
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + token,
      }
    })
  } else {
    return axios.create({
      baseURL: `${REQUEST_URI}/`,
      timeout: 10000,
    })
  }
}