import {StyleSheet} from 'react-native'
import {Colors} from 'react-native-ui-lib'

export const appColors = {
  primaryColor: '#669947',
  primaryColorLight: Colors.yellow50,
  primaryColorDark: Colors.yellow10,
  secondaryColor: '#364c92',
  secondaryColorLight: '#666',
  secondaryColorDark: Colors.violet10,
  textColor: Colors.grey10,
  textColorInverted: Colors.white,
  iconColor: '#c44f2d',
  errorColor: Colors.red40,
  successColor: Colors.green10,
  warnColor: Colors.red50,
  lightBackground: '#fefbf6',
  darkBackground: Colors.dark10,
  modalBackgroundColor: Colors.rgba(Colors.dark30, 0.5),
}

export const addTypography = {
  heading: {fontSize: 24, fontWeight: '600'},
  subheading: {fontSize: 20, fontWeight: '500'},
  body: {fontSize: 16, fontWeight: '400'},
  subtext: {fontSize: 12, fontWeight: '300'},
}

export const styles = StyleSheet.create({
  singleTextInput: {
    borderBottomColor: appColors.textColor,
    borderBottomWidth: 1,
    borderStyle: 'solid',
    fontSize: 20,
    paddingBottom: 5,
    height: 38,
    color: appColors.textColor,
    fontWeight: 'normal',
    textAlign: 'center',
  },
  mapToastIconWrap: {
    backgroundColor: appColors.lightBackground,
    width: 25,
    height: 25,
    textAlign: 'center',
    borderRadius: 3,
    marginRight: 10
  },
  mapToastIcon: {
    fontSize: 25,
    lineHeight: 25,
    color: appColors.textColor,
  },
  mapTopButtons: {
    width: 40,
    height: 40,
    borderRadius: 20,
    position: 'absolute',
    right: 20,
    top: 20,
    backgroundColor: Colors.rgba(appColors.iconColor, 0.7),
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuButton: {
    position: 'absolute',
    top: 20,
    left: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: Colors.rgba(appColors.iconColor, 0.7),
    height: 40,
    width: 50,
  },
})