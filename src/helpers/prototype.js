Array.prototype.getGoogleAddressFromList = function(index) {
  let fullAddress = ''
  
  this.forEach( (coordItem, i) => {
    let streetNum = '',
        street = ''

    if( i == index ) {
      coordItem.address_components.forEach( addrComp => {
        if(addrComp.types) {
          if( addrComp.types.includes('street_number') ) streetNum = addrComp.long_name
          if( addrComp.types.includes('route') ) street = addrComp.long_name

          fullAddress = street + ', ' + streetNum
        }
      } )
    }
  } )
  
  return fullAddress
}
