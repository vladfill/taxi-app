export const makeClearPhone = phone => phone.replace(/\(|\)| /g, '')

export const getLetterByIndex = i => {
  if (i <= 5) {
    const arr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']
    return arr[i]
  }

  return false
}

export const convertMetersToKm = m => (m/1000).toFixed(1)

export const convertSecondsToKm = sec => {
  let hours = Math.floor(sec / 3600),
      secPart = sec % 3600,
      minutes = Math.floor(secPart / 60),
      seconds = secPart % 60

  minutes += (seconds > 30) ? 1 : 0

  return `${hours}ч ${minutes}m`
}

export const getIconByPlaceType = types => {
  if( !types && !Array.isArray(types)  ) return 'place'

  switch (types[0]) {
    case types.includes('airport'):
      return 'airport'

    case types.includes('bar'):
      return 'bar'

    case types.includes('bus_station'):
      return 'directions-bus'

    case types.includes('cafe'):
      return 'cafe'

    case types.includes('car_dealer'):
    case types.includes('car_rental'):
    case types.includes('car_repair'):
      return 'directions-car'

    case types.includes('car_wash'):
      return 'car-wash'

    case types.includes('casino'):
      return 'casino'

    case types.includes('hospital'):
      return 'hospital'

    case types.includes('restaurant'):
      return 'restaurant'

    case types.includes('store'):
      return 'mall'

    case types.includes('museum'):
      return 'museum'

    case types.includes('parking'):
      return 'parking'

    case types.includes('train_station'):
      return 'train'

    case types.includes('school'):
    case types.includes('university'):
    case types.includes('secondary_school'):
      return 'school'

    default:
      return 'place'
  }
}

export const checkIfObjectNotEmpty = ( obj ) => Object.entries(obj).length !== 0 && obj.constructor === Object

export const getGoogleAddress = (gaObj) => {
  if( ! gaObj.address_components ) return ''

  let streetNum = '',
      street = ''

  gaObj.address_components.forEach( addrComp => { //street_number, route
      if(addrComp.types) {
      if( addrComp.types.includes('street_number') ) streetNum = addrComp.long_name
      if( addrComp.types.includes('route') ) street = addrComp.long_name
      }
  } )

  return street + ', ' + streetNum
}
