export const API_URL = 'http://10.0.2.2:1323'
// export const API_URL = 'http://vladfill.herokuapp.com'
export const API_VERSION = 'v1'
export const REQUEST_URI = API_URL + "/" + API_VERSION

export const WS_URL = 'ws://10.0.2.2:1323'
// export const WS_URL = 'wss://vladfill.herokuapp.com'
export const WS_REQUEST_URI = WS_URL + "/" + API_VERSION + "/ws"