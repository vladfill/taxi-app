import React, {useState} from 'react'
import {
  TouchableOpacity,
  TextField,
  View,
  Text,
} from 'react-native-ui-lib'
import Icon from 'react-native-vector-icons/MaterialIcons'

import {appColors} from '../../../assets/styles'

const OrderTextField = (props) => {
  const {
    placeholder, 
    value, 
    errorMessage, 
    styles, 
    enableErrors, 
    onPress, 
    markerLetter,
    icon,
    rightIcon,
    rightIconHandler,
  } = props
  const [fieldWidth, setFieldWidth] = useState(280)
  
  return (
    <TouchableOpacity 
      style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', position: 'relative'}} 
      onLayout={(event) => (markerLetter || icon) ? setFieldWidth(event.nativeEvent.layout.width - 40) : null}
      onPress={() => (typeof onPress === 'function') ? onPress() : null}>

      {
        ( markerLetter ) ?
          (
            <View center bg-iconColor style={{
              height: 32,
              width: 32,
              borderRadius: 16,
            }}>
              <Text textColorInverted>{markerLetter}</Text>
            </View>
          ) : ( icon ) ?
          (
            <View center bg-iconColor style={{
              height: 32,
              width: 32,
              borderRadius: 16,
            }}>
              <Icon 
                size={20}
                color={appColors.textColorInverted}
                name={icon}
              />
            </View>
          ) : null
      }
      
      <View style={ (markerLetter || icon) ? {width: fieldWidth} : {width: '100%'}}>
        <TextField
          key={'not-centered'}
          placeholder={placeholder}
          value={value}
          underlineColor={appColors.textColorInverted}
          floatingPlaceholder={false}
          style={ styles ? [{paddingLeft: 10, fontSize: 15}, styles] : {paddingLeft: 10, fontSize: 15}}
          placeholderTextColor={appColors.textColorInverted}
          editable={false}
          enableErrors={enableErrors ? true : false}
          multiline={true}
          rightIconStyle={{height: 30, width: 30}}
          error={errorMessage}
        />
      </View>

      {
        (rightIcon && rightIconHandler) ?
        (<Icon 
          style={{
            position: 'absolute',
            right: 0,
            bottom: 10,
            zIndex: 20,
          }}
          size={20}
          onPress={() => rightIconHandler()}
          color={appColors.textColorInverted}
          name={rightIcon}
        />) : null
      }
    </TouchableOpacity>
  )
}

export default OrderTextField