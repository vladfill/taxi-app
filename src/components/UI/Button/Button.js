import React from 'react'
import {
  Button,
} from 'react-native-ui-lib'

const CustomButton = props => {
  const {label, onPress, backgroundColor, color} = props

  return (
    <Button 
      backgroundColor={backgroundColor}
      color={color}
      size='large'
      style={{
        minWidth: 200,
        marginTop: 20,
      }}
      labelStyle={{
        fontSize: 20,
        paddingLeft: 20,
        paddingRight: 20,
      }}
      onPress={() => (typeof onPress === 'function') ? onPress() : null}
      label={label} />
  )
}

export default CustomButton