import React, {Component, createRef} from 'react'
import {connect} from 'react-redux'
import {
  StyleSheet,
  Dimensions,
  Animated,
  Image,
} from 'react-native'
import {
  Text,
  View,
  Toast,
  TouchableOpacity,
} from 'react-native-ui-lib'
import MapView, 
{
  ProviderPropType,
  Marker,
  AnimatedRegion,
  Polyline,
  PROVIDER_GOOGLE,
} from 'react-native-maps'
import Geolocation from 'react-native-geolocation-service'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import polyline from '@mapbox/polyline'
import axiosLib from 'axios'

import {appColors, styles as commonStyles} from '../../assets/styles'
import {retroMapStyle} from '../../assets/mapStyles/retro'
import {getLetterByIndex, convertMetersToKm, getGoogleAddress} from '../../helpers'

import Form from './Form/Form'

const screen = Dimensions.get('window'),
      ASPECT_RATIO = screen.width / screen.height,
      LATITUDE_DELTA = 0.0922/2,
      LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO,
      GOOGLE_MAP_API_KEY = 'AIzaSyCN3MNoZWUJI_Kx_Sn9MX-h4hVN_Se7qBs'

class Order extends Component {
  constructor(props) {
    super(props)

    this.state = {
      iconSize: 25,
      showToast: false,

      distanceRight: new Animated.Value(-100),
      priceLeft: new Animated.Value(-100),

      // Order data
      search: '',
      price: '',
      message: '',
      childChair: false,
      moreThat4Seats: false,

      // Coordinates
      coordsList: [],
      direction: [],
      possibleCoord: {},
      possibleCoordFromMapAPI: {},
      orderDistance: 0,
      orderDuration: 0,

      formHeight: 0,

      watcher: false,
    };

    this.map = createRef()
    this.watchId == null
  }

  changeTextHandler = (query, stateName) => {
    this.setState({
      [stateName]: query,
    })
  }

  onMapReadyHandler = () => {
    Geolocation.getCurrentPosition(info => {
      info.coords.latitude = 49.570811
      info.coords.longitude = 34.49543

      const {latitude, longitude} = info.coords
      const coordinate = {
        latitude: latitude,
        longitude: longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }

      setTimeout(()=>{
        this.map.animateCamera({center: coordinate, zoom: 17}, 5000)
        this.setState({
          location: {...info.coords},
          watcher: true,
        })
      }, 1000)
    })

    this.map.animateToViewingAngle(30, 2000)
  }

  updateCoordsList = (coord, i=null) => {
    const coordsList = [...this.state.coordsList]
    if( i || i === 0 ) {
      coordsList[i] = {...coord}
    } else {
      coordsList.push({...coord})
    }

    this.setState({coordsList})
  }

  removeItemFromCoordsList = (i) => {
    const coordsList = [...this.state.coordsList]
    coordsList.splice(i, 1)
    this.setState({coordsList})
  }

  mapPressHandler = (e) => {
    const {coordinate, position} = e.nativeEvent

    if( ! coordinate ) return

    this.hideDistanceMessage()

    this.setState({possibleCoord: coordinate})
    this.map.animateCamera({center: {
      latitude: coordinate.latitude,
      longitude: coordinate.longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    }}, 5000)
    this.reverseGeolocationRequest(coordinate)
  }

  onAddAddressHandler = () => {
    const coordsList = [...this.state.coordsList]
    coordsList.push(this.state.possibleCoordFromMapAPI)
    this.setState({
      coordsList,
      possibleCoord: {},
      possibleCoordFromMapAPI: {},
      showToast: false,
    })

    this.showDistanceMessage()
  }

  onRemoveAddressHandler = () => {
    this.setState({
      possibleCoord: {},
      possibleCoordFromMapAPI: {},
      showToast: false,
    })

    this.showDistanceMessage()
  }

  currentLocationAnimation = () => {
    Geolocation.getCurrentPosition(info => {
      // info.coords.latitude = 49.570811
      // info.coords.longitude = 34.49543
      const {latitude, longitude} = info.coords
      const latitudeDelta = 0.0922 / 10;
      const longitudeDelta = latitudeDelta * ASPECT_RATIO;
      const coordinate = {
        latitude: latitude,
        longitude: longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }

      this.map.animateCamera({center: coordinate}, 5000)
    })
  }

  reverseGeolocationRequest = ({latitude, longitude}) => {
    const latLng = latitude + ',' + longitude,
          lang = 'ru'

    axiosLib.get(`https://maps.googleapis.com/maps/api/geocode/json?&key=${GOOGLE_MAP_API_KEY}&latlng=${latLng}&language=${lang}`)
    .then( (response) => {
      if(response.data.status == 'OK' && response.data.results.length) {
        const firstResult = response.data.results[0]

        if( firstResult.address_components.length ) {
          this.setState({
            possibleCoordFromMapAPI: firstResult,
            showToast: true,
          })
        }
      }
    })

    .catch((error) => {
      console.log(error)
    })
  }
  
  directionRequest = () => {
    const coordsList = this.state.coordsList.map(coord => JSON.parse(JSON.stringify(coord))),
          origin = Object.values(coordsList.shift().geometry.location).join(','),
          destination = Object.values(coordsList.pop().geometry.location).join(',')

    let request = `https://maps.googleapis.com/maps/api/directions/json?mode=driving&key=${GOOGLE_MAP_API_KEY}&origin=${origin}&destination=${destination}`,
        waypoints = []

    coordsList.forEach(coord => {
      waypoints.push(Object.values(coord.geometry.location).join(','))
    })

    if(waypoints.length) {
      request += `&waypoints=${waypoints.join('|')}`
    }

    axiosLib.get(request)
    .then( (response) => {
      if(response.data.status == 'OK' && response.data.routes.length) {
        const firstRoute = response.data.routes[0],
              points = firstRoute.overview_polyline.points,
              coords = polyline.decode(points)

        let orderDistance = 0,
            orderDuration = 0

        if(points && coords.length){
          firstRoute.legs.forEach(leg => {
            const {distance, duration} = leg

            if(distance && distance.value) {
              orderDistance += distance.value
            }

            if(duration && duration.value) {
              orderDuration += duration.value
            }
          })

          this.setState({
            direction: coords,
            orderDistance,
            orderDuration,
          })

          const coordsArr = coords.map( coord => {
            return {
              latitude: coord[0],
              longitude: coord[1],
            }
          } )

          this.map.fitToCoordinates(coordsArr, {
            animated: true,
            edgePadding: {
              top: 20,
              bottom: 20,
              left: 20,
              right: 20,
            }
          })
        } 
      }
    })
    .catch((error) => {
      console.log(error)
    })
  }

  showDistanceMessage = () => {
    Animated.timing(
      this.state.distanceRight,     
      {toValue: 0, duration: 500, useNativeDriver: false},
    ).start();
  }
  
  hideDistanceMessage = () => {
    Animated.timing(
      this.state.distanceRight,     
      {toValue: -100, duration: 500, useNativeDriver: false},
    ).start();
  }

  showPriceMessage = () => {
    Animated.timing(
      this.state.priceLeft,     
      {toValue: 0, duration: 500, useNativeDriver: false},
    ).start();
  }
  
  hidePriceMessage = () => {
    Animated.timing(
      this.state.priceLeft,     
      {toValue: -100, duration: 500, useNativeDriver: false},
    ).start();
  }

  setFormHeight = (formHeight) => this.setState({formHeight})

  render() {
    // Marker for possible coords
    let possibleCoord = null
    if( this.state.possibleCoord.latitude && this.state.possibleCoord.longitude ) {
      const coordinate ={
        latitude: this.state.possibleCoord.latitude,
        longitude: this.state.possibleCoord.longitude,
      }

      possibleCoord = (
        <Marker
          coordinate={coordinate}
          key={'confirm-marker'}
          tracksViewChanges={false}
        >
          <View style={{
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            }}>
            <View>
              <Image
                style={{width: 25, height: 40}}
                source={require('../../assets/images/direction-marker-confirm.png')}
              />
            </View>
          </View>
        </Marker>
      )
    }

    let coordsList = []
    if( this.state.coordsList.length ) {
      coordsList = this.state.coordsList.map( (coord, i) => {
        const {lat, lng} = coord.geometry.location
        const letter = getLetterByIndex(i)
        const coordinate = {
          latitude: lat,
          longitude: lng,
      }

      return (
        <Marker
          coordinate={coordinate}
          key={'direction-marker-' + letter}
          tracksViewChanges={false}
        >
          <View style={{position: 'relative'}}>
            <Image
              style={{width: 25, height: 40}}
              source={require('../../assets/images/direction-marker.png')} 
            />
            <Text style={{
              position: 'absolute', 
              top: 0, 
              left: 0,
              width: '100%',
              textAlign: 'center',
              fontSize: 16,
            }}>{letter}</Text>
          </View>
        </Marker>)
      } )
    }

    // Add address for toast
    let toastAddress = ''
    if( this.state.possibleCoordFromMapAPI && this.state.showToast ) {
      /* let streetNum = '',
          street = ''

      this.state.possibleCoordFromMapAPI.address_components.forEach( addrComp => { //street_number, route
        if(addrComp.types) {
          if( addrComp.types.includes('street_number') ) streetNum = addrComp.long_name
          if( addrComp.types.includes('route') ) street = addrComp.long_name
        }
      } ) */

      toastAddress = getGoogleAddress(this.state.possibleCoordFromMapAPI)
    }

    // Derction polyline
    let direction = null
    if(this.state.direction.length > 2) {
      const directionCoords = this.state.direction.map( coord => {
        return {
          latitude: coord[0],
          longitude: coord[1],
        }
      } )

      // For view like one polyline with white borders
      direction = []
      direction.push(
        <Polyline
          key={'outer-polyline'}
          coordinates={directionCoords}
          strokeWidth={6}
          strokeColor={appColors.primaryColorLight} />
      )
      direction.push(
        <Polyline
          key={'inner-polyline'}
          coordinates={directionCoords}
          strokeWidth={4}
          strokeColor={appColors.primaryColor} />
      )
    }

    let orderDistance = null
    if( this.state.orderDistance ) {
      orderDistance = (
        <Animated.View style={{
          position: 'absolute',
          bottom: 20,
          right: this.state.distanceRight,
          width: 100,
          paddingLeft: 25,
          paddingRight: 10,
          borderTopLeftRadius: 20,
          borderBottomLeftRadius: 20,
          borderWidth: 1,
          borderColor: appColors.darkBackground,
          backgroundColor: appColors.primaryColor,
        }}>
          <Text style={{
            textAlign: 'center',
            fontSize: 18,
            height: 40,
            lineHeight: 40,
            color: appColors.textColorInverted,
          }}>{convertMetersToKm(this.state.orderDistance)} km</Text>
        </Animated.View>
      )
    }

    let price = null
    if( this.state.price ) {
      price = (
        <Animated.View style={{
          position: 'absolute',
          bottom: 20,
          left: this.state.priceLeft,
          width: 100,
          paddingLeft: 10,
          paddingRight: 25,
          borderTopRightRadius: 20,
          borderBottomRightRadius: 20,
          borderWidth: 1,
          borderColor: appColors.darkBackground,
          backgroundColor: appColors.primaryColor,
        }}>
          <Text style={{
            textAlign: 'center',
            fontSize: 18,
            height: 40,
            lineHeight: 40,
            color: appColors.textColorInverted,
          }}>{this.state.price} грн.</Text>
        </Animated.View>
      )
    }

    return(
      <View flex>
        <View style={{position: 'relative', height: screen.height-this.state.formHeight}}>
          {orderDistance}
          <MapView
            ref={map => this.map = map}
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            initialRegion={{
              latitude: this.props.cityCoords.latitude,
              longitude: this.props.cityCoords.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }}
            showsUserLocation={true}
            useNavigateDriver={true}
            loadingEnabled={true}
            showsMyLocationButton={false}
            showsCompass={false}
            maxZoomLevel={18}
            customMapStyle={retroMapStyle}
            mapPadding={{
              top: 10,
              right: 10,
              bottom: 10,
              left: 10,
            }}
            onMapReady={this.onMapReadyHandler}
            onPress={this.mapPressHandler}
            //onRegionChangeComplete={this.onRegionChangeComplete}
            //onRegionChange={this.onRegionChange}
          >
            
            {/* Added for normal render marker images */}
            <Marker
              coordinate={{latitude: 0, longitude: 0}}
              key={'confirm-marker'}
              tracksViewChanges={false}
            >
              <View style={{display: 'none'}}>
                <Image
                  style={{width: 1, height: 1}}
                  source={require('../../assets/images/direction-marker-confirm.png')}
                />
                <Image
                  style={{width: 1, height: 1}}
                  source={require('../../assets/images/direction-marker.png')}
                />
                <Image
                  style={{width: 1, height: 1}}
                  source={require('../../assets/images/car-small.png')} 
                />
              </View>
            </Marker>

            {possibleCoord}
            {coordsList}
            {direction}
          </ MapView>

          {/* Menu button */}
          <TouchableOpacity style={commonStyles.menuButton} onPress={this.props.navigation.toggleDrawer}>
            <Icon name="menu" size={30} color={appColors.textColorInverted} />
          </TouchableOpacity>

          {/* GPS button */}
          <TouchableOpacity style={commonStyles.mapTopButtons} onPress={this.currentLocationAnimation}>
            <Icon name="crosshairs-gps" size={30} color={appColors.textColorInverted} />
          </TouchableOpacity>

          {orderDistance}
          {price}

          {/* Address toast for confirm */}
          <Toast
            visible={this.state.showToast}
            position={'top'}
            backgroundColor={appColors.primaryColor}
          >
            <View padding-s2 style={{
              display: 'flex',
              flexWrap: 'wrap',
              flexDirection: 'row',
              justifyContent: 'space-between'}}
            >
              <Text textColorInverted body>{toastAddress}</Text>
              <View style={{
                display: 'flex',
                flexDirection: 'row',}}
              >
                <TouchableOpacity style={[commonStyles.mapToastIconWrap, {marginRight: 10}]} onPress={() => this.onAddAddressHandler()}>
                  <Icon 
                    name='plus'
                    style={commonStyles.mapToastIcon} />
                </TouchableOpacity>
                <TouchableOpacity style={commonStyles.mapToastIconWrap} onPress={() => this.onRemoveAddressHandler()}>
                  <Icon 
                    name='minus'
                    style={commonStyles.mapToastIcon} />
                </TouchableOpacity>
              </View>
            </View>
          </Toast>
        </View>

        <Form 
          setFormHeight={h => this.setFormHeight(h)} 
          search={this.state.search}
          price={this.state.price}
          message={this.state.message}
          childChair={this.state.childChair}
          moreThat4Seats={this.state.moreThat4Seats}
          coordsList={this.state.coordsList}
          changeTextHandler={this.changeTextHandler}
          updateCoordsList={this.updateCoordsList}
          removeItemFromCoordsList={this.removeItemFromCoordsList}
        />
      </View>
    )
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.coordsList.length > 1 && this.state.coordsList != prevState.coordsList) {
      this.directionRequest()
    } else if(this.state.coordsList.length === 1 && this.state.coordsList != prevState.coordsList) {
      const firstCoord = this.state.coordsList[0],
            {lat, lng} = firstCoord.geometry.location

      if(lat && lng) {
        this.map.animateCamera({center: {
          latitude: lat,
          longitude: lng,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }}, 5000)
      }
    }

    if(prevState.orderDistance !== this.state.orderDistance && this.state.orderDistance) {
      this.showDistanceMessage()
    } 

    if(prevState.price !== this.state.price && this.state.price) {
      this.showPriceMessage()
    } else if(prevState.price !== this.state.price && !this.state.price) {
      this.hidePriceMessage()
    }
  }
}

const stateToProps = state => {
  return {
    cityCoords: state.mainInfo.cityCoords,
  }
}

const dispToProps = dispatch => {
  return {
    updateMainInfo: payload => dispatch({type: types.UPDATE_MAIN_INFO, payload}),
  }
}

export default connect(stateToProps, dispToProps)(Order)

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
    width: '100%',
  },
})