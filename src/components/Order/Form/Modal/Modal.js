import React, {Component} from 'react'
import {
  TextInput, 
  StyleSheet,
  Dimensions,
  Keyboard,
  Animated,
} from 'react-native'
import {connect} from 'react-redux'
import {
  Text,
  View,
  Switch,
  TouchableOpacity,
} from 'react-native-ui-lib'
import Modal from "react-native-modal"
import AsyncStorage from '@react-native-community/async-storage'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { v3 as uuidv3 } from 'uuid'
import axiosLib from 'axios'
import {getIconByPlaceType} from '../../../../helpers'
import Button from '../../../UI/Button/Button'
import {appColors} from '../../../../assets/styles'

const GOOGLE_MAP_API_KEY = 'AIzaSyCN3MNoZWUJI_Kx_Sn9MX-h4hVN_Se7qBs'

class ModalMapPage extends Component {
  state = {
    searchResults: [],
    savedSearchResults: [],
    resultsHeight:  new Animated.Value(0),
    sessionToken: '',
  }

  saveAddress = async (value, key) => {
    const savedSearchResults = [...this.state.savedSearchResults]

    let flag = true
    savedSearchResults.forEach(result => {
      if(result.place_id === value.place_id) {
        flag = false
      }
    })

    if( flag ) {
      savedSearchResults.push(value)
      this.setState({savedSearchResults})

      try {
        await AsyncStorage.setItem(key, JSON.stringify(savedSearchResults))
      } catch (e) {
        console.log(e);
      }
    }
  }

  deleteAddress = async (index, key) => {
    const savedSearchResults = [...this.state.savedSearchResults]
    savedSearchResults.splice(index, 1)
    this.setState({savedSearchResults})

    try {
      await AsyncStorage.setItem(key, JSON.stringify(savedSearchResults))
    } catch (e) {
      console.log(e);
    }
  }

  keyboardDidShow = (e) => {
    const height = Dimensions.get('window').height - e.endCoordinates.height - 150
    Animated.timing(
      this.state.resultsHeight,
      {toValue: height, duration: 1, useNativeDriver: false},
    ).start();
  }
  
  keyboardDidHide = () => {
    const height = Dimensions.get('window').height - 150
    Animated.timing(
      this.state.resultsHeight,
      {toValue: height, duration: 1, useNativeDriver: false},
    ).start();
  }

  getAddress = () => {
    if( this.props.search.length < 3 ) return

    const location = this.props.cityCoords.latitude + ',' + this.props.cityCoords.longitude
    let request = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${this.props.search}&key=${GOOGLE_MAP_API_KEY}`
        request += `&components=country:ua&location=${location}&types=address&radius=30000`
                
    axiosLib.get(request)
    .then( (response) => {
      if(response.data.status == 'OK' && response.data.predictions.length) {
        this.setState({
          searchResults: [...response.data.predictions]
        })
      }
    })
    .catch((error) => {
      console.log(error)
    })
  }

  getAddressDetails = (placeID) => {
    if( ! placeID ) return

    let request = `https://maps.googleapis.com/maps/api/place/details/json?key=${GOOGLE_MAP_API_KEY}&placeid=${placeID}`

    axiosLib.get(request)
    .then( (response) => {
      console.log(response.data);
      if(response.data.status == 'OK' && response.data.result) {
        this.props.updateCoordsList(response.data.result)
        this.props.hideModal()
      }
    })
    .catch((error) => {
      console.log(error)
    })
  }

  updateSearchAddress = ( address ) => {
    if( ! Array.isArray(address.types) ) return
    const newSearchTypes = ['route', 'locality', 'sublocality_level_1']
    let isFinalSearch = true

    for (let i = 0; i < address.types.length; i++) {
      const type = address.types[i]

      if( newSearchTypes.includes(type) ) isFinalSearch = false
    }

    if( !isFinalSearch ) {
      const updatedAddress = address.structured_formatting.main_text + ', '
      this.props.changeTextHandler(updatedAddress, 'search')
    } else {
      this.saveAddress(address, '@savedSearchResults2')
      const {place_id} = address
      this.getAddressDetails(place_id)
    }
  }

  getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key)
      if (value !== null) {
        this.setState({savedSearchResults: JSON.parse(value)})
      } else {
        console.log('getData error');
      }
    } catch(e) {
      console.log(e);
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow,
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide,
    )
  }

  componentDidUpdate(prevProps) {
    if(prevProps.search !== this.props.search && this.props.search) {
      if( this.props.search.length > 2 ) {
        this.getAddress()
      }
    }
  }

  render() {
    const {search, price, message, childChair, moreThat4Seats} = this.props,
          {searchResults, savedSearchResults} = this.state,
          helpMessageComponent = ((search.length < 3 && search.length) > 0 && (this.props.dialogType == 'from' || this.props.dialogType == 'to')) 
          ? <Text style={styles.helpMessage}>Продолжайте вводить для автозаполнения</Text> : null

    let modalFields = null

    switch (this.props.dialogType) {
      case 'from':
      case 'to':
        modalFields = (
          <View style={{
            backgroundColor: '#F8F8F8',
            padding: 10,
            marginBottom: 10,
          }}>
            <View style={{
              backgroundColor: '#fff',
              position: 'relative',
            }}>
              <TextInput 
                ref={input => this.input = input}
                blurOnSubmit={false}
                placeholder={'Введите адрес'} 
                value={(search) ? search : ''} 
                onChangeText={(e) => this.props.changeTextHandler(e, 'search')}
              />
              {
                (this.props.search) ?
                (<Icon 
                  name="close"
                  onPress={() => this.props.changeTextHandler('', 'search')}
                  style={{
                    fontSize: 20,
                    color: '#5EBA7D',
                    position: 'absolute',
                    top: 15,
                    right: 5,
                  }} />) : null
              }
            </View>
          </View>
        )
        break
      
      case 'aditional':
        modalFields = (
          <View>
            <TextInput 
              placeholder={'Price'} 
              value={price} 
              keyboardType={'numeric'}
              onChangeText={(e) => this.props.changeTextHandler(e, 'price')}
            />
            <TextInput 
              placeholder={'Message'} 
              value={message} 
              onChangeText={(e) => this.props.changeTextHandler(e, 'message')}
            />
            <View style={styles.switersWraper}>
              <Text>Child chair</Text>
              <Switch value={childChair} onValueChange={value => this.props.changeTextHandler(value, 'childChair')} />
            </View>
            <View style={styles.switersWraper}>
              <Text>More that 4 seats</Text>
              <Switch value={moreThat4Seats} onValueChange={value => this.props.changeTextHandler(value, 'moreThat4Seats')} />
            </View>
            <View style={{marginTop: 20}}>
              <Button 
                backgroundColor={appColors.secondaryColor}
                color={appColors.textColorInverted}
                onPress={() => this.saveOrderToStore()}
                label="Сохранить" />
            </View>
          </View>
        )
        break
    }

    let searchResultsItems = null
    const isSearchResults = searchResults.length && search.length,
          searchItems = (isSearchResults) ? searchResults : savedSearchResults

    if( this.props.dialogType == 'from' || this.props.dialogType == 'to' ) {
      searchResultsItems = searchItems.map( (item, index) => {
        const {structured_formatting, types} = item,
              iconName = getIconByPlaceType(types),
              key = structured_formatting.main_text + ', ' + structured_formatting.secondary_text

        return (
          <TouchableOpacity key={key} onPress={() => this.updateSearchAddress(item)}>
            <View style={styles.autocompleteAddress}>
              <Icon 
                name={iconName}
                style={{
                  fontSize: 30,
                  color: '#5EBA7D',
                  paddingRight: 10,
                }} />
                <View>
                  <Text style={{fontSize: 15}}>{structured_formatting.main_text}</Text>
                  <Text style={{fontSize: 13, color: '#999'}}>{structured_formatting.secondary_text}</Text>
                </View>
                { (! isSearchResults) ? 
                (<Icon 
                  name="close"
                  onPress={() => this.deleteAddress(index, '@savedSearchResults2')}
                  style={{
                    fontSize: 20,
                    color: '#5EBA7D',
                    position: 'absolute',
                    top: 15,
                    right: 0,
                  }} />) : null}
            </View>
          </TouchableOpacity>
        )
      } )
    }

    return(
      <View style={{...styles.container, marginTop: 15}}>
        <Modal
          animationType="fade"
          transparent
          isVisible={this.props.showDialog}
          swipeDirection={['left', 'right']}
          onSwipeComplete={() => this.props.hideModal()}
          backdropColor={appColors.darkBackground}
          scrollEnabled={false}
          scrollHorizontal={true}
          animationInTiming={500}
          onModalHide={() => {
            this.setState({searchResults: []})
            this.props.changeTextHandler('', 'search')
          }}
          onModalWillShow={() => {
            this.getData('@savedSearchResults2')
          }}
          onModalShow={() => {
            if( this.props.search ) {
              this.getAddress()
            }
          }}
        >
          <View style={{...styles.container, paddingLeft: 10, paddingRight: 10, paddingTop: 35, backgroundColor: '#fff'}}>
            <TouchableOpacity 
              style={{
                position: 'absolute',
                right: 10,
                top: 10,
              }} >
              <Icon 
                size={25}
                color={appColors.textColor}
                name="close"
                onPress={() => this.props.hideModal()}
              />
            </TouchableOpacity>

            <View>
              {modalFields}
              <Animated.ScrollView keyboardShouldPersistTaps={'handled'} style={{height: this.state.resultsHeight}}>
                <View style={{
                  backgroundColor: '#F8F8F8',
                  padding: 10,
                }}>
                  {helpMessageComponent ? helpMessageComponent : searchResultsItems}
                </View>
              </Animated.ScrollView>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const stateToProps = state => {
  return {
    cityCoords: state.mainInfo.cityCoords,
  }
}

export default connect(stateToProps)(ModalMapPage)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  helpMessage: {
    textAlign: 'center',
    lineHeight: 25,
    textAlignVertical: 'center',
    backgroundColor: '#999',
    color: '#fff',
  },
  autocompleteAddress: {
    textAlign: 'center',
    lineHeight: 40,
    textAlignVertical: 'center',
    borderBottomColor: appColors.textColorInverted,
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 2,
    paddingTop: 5,
    paddingBottom: 5,
  },
  switersWraper: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    height: 40,
  },
})