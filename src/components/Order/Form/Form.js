import React, {Component} from 'react'
import {
  ScrollView
} from 'react-native'
import {
  Text,
  View,
  Dialog,
} from 'react-native-ui-lib'
import Modal from './Modal/Modal'

import OrderTextField from '../../UI/OrderTextField/OrderTextField'
import Button from '../../UI/Button/Button'

import {appColors} from '../../../assets/styles'
import {getLetterByIndex, getGoogleAddress} from '../../../helpers'

class Form extends Component {
  state = {
    showDialog: false,
    dialogType: null,
    errorMessage: 'Это поле обязательно для заполнения',
  }

  openModal = (dialogType) => this.setState({showDialog: true, dialogType})
  hideModal = () => this.setState({showDialog: false, dialogType: null})

  renderPannableHeader = props => {
    const {title} = props

    return (
      <View>
        <View margin-20>
          <Text>{title}</Text>
        </View>
        <View height={2} bg-dark70/>
      </View>
    )
  }

  render() {
    const {message, childChair, moreThat4Seats, coordsList} = this.props
    let additional = [],
        fromFields = (
          <OrderTextField 
            markerLetter={'B'}
            placeholder={'Куда'} 
            value={coordsList.getGoogleAddressFromList(1)}
            onPress={() => {
              if( coordsList.getGoogleAddressFromList(1) ) this.props.changeTextHandler(coordsList.getGoogleAddressFromList(1), 'search')
              this.openModal('to')
            }}
            styles={{marginTop: 10}}
            rightIcon={ (coordsList.getGoogleAddressFromList(1)) ? 'add' : null }
            rightIconHandler={ (coordsList.getGoogleAddressFromList(1)) ? () => this.openModal('to') : null}
            errorMessage={this.state.errorMessage} />
        )

    if( coordsList.length > 2 ) {
      fromFields = coordsList.filter((coordItem, i) => i > 0).map( (coordItem, i) => {  
        return (
          <OrderTextField 
            markerLetter={getLetterByIndex(i+1)}
            placeholder={'Куда'} 
            value={getGoogleAddress(coordItem)}
            onPress={() => {
              if( getGoogleAddress(coordItem) ) this.props.changeTextHandler(getGoogleAddress(coordItem), 'search')
              this.openModal('to')
            }}
            styles={{marginTop: 10}}
            rightIcon={ (coordsList.length == i+2) ? 'add' : 'close' }
            rightIconHandler={ (coordsList.length == i+2) ? () => this.openModal() : () => this.props.removeItemFromCoordsList(i+1) }
            errorMessage={this.state.errorMessage} />
        )
      } )
    }

    if( message ) additional.push(message)
    if( childChair ) additional.push('Детское кресло')
    if( moreThat4Seats ) additional.push('Больше 4 сидений')

    return (
      <View bg-primaryColor paddingH-s5 paddingT-s3 pa onLayout={(event) => {
        const {height} = event.nativeEvent.layout
        this.props.setFormHeight(height)
      }}>
        <View>
          <OrderTextField 
            markerLetter={'A'}
            placeholder={'Откуда'} 
            value={coordsList.getGoogleAddressFromList(0)}
            onPress={() => {
              if( coordsList.getGoogleAddressFromList(0) ) this.props.changeTextHandler(coordsList.getGoogleAddressFromList(0), 'search')
              this.openModal('from')
            }}
            rightIcon={ (coordsList.getGoogleAddressFromList(0)) ? 'close' : null }
            rightIconHandler={ (coordsList.getGoogleAddressFromList(0)) ? () => this.props.removeItemFromCoordsList(0) : null }
            errorMessage={this.state.errorMessage} />

          {fromFields}

          <OrderTextField 
            icon={'view-list'}
            placeholder={'Дополнительно'} 
            value={additional.join('. ')}
            onPress={() => this.openModal('aditional')}
            styles={{marginTop: 10}}
            errorMessage={this.state.errorMessage} />
          <Button 
            backgroundColor={appColors.secondaryColor}
            color={appColors.textColorInverted}
            onPress={() => BackHandler.exitApp()}
            label="Заказать" />
        </View>

        <Modal 
          openModal={this.openModal}
          hideModal={this.hideModal}
          showDialog={this.state.showDialog}
          dialogType={this.state.dialogType}

          search={this.props.search}
          price={this.props.price}
          message={this.props.message}
          childChair={this.props.childChair}
          moreThat4Seats={this.props.moreThat4Seats}
          coordsList={this.props.coordsList}
          changeTextHandler={this.props.changeTextHandler}
          updateCoordsList={this.props.updateCoordsList}
        />
      </View>
    )
  }

  addText = () => {
    this.setState({text: this.state.text + this.state.text})
  }
}

export default Form