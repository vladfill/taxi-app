/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {Component} from 'react'
import { 
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer'
import {Text} from 'react-native-ui-lib'
import { NavigationContainer } from '@react-navigation/native'

import Login from '../Login/Login'
import Order from '../Order/Order'

const hoc = (WrappedComponent, newProps={}) => {
  return (props) => {
    return <WrappedComponent {...props} {...newProps} />
  }
}

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>
      <Text>Text</Text>
      <DrawerItemList {...props} />
      <DrawerItem label="Help" onPress={() => alert('Link to help')} />
    </DrawerContentScrollView>
  );
}

const Drawer = createDrawerNavigator()

const DrawerComponent = (props) => {
  const newProps = {
    storageData: props.storageData,
    saveStorageData: props.saveStorageData,
  }

  return (
    <NavigationContainer>
      <Drawer.Navigator 
        initialRouteName="Login" 
        drawerType="slide"
        drawerContent={newProps => <CustomDrawerContent {...newProps} />}>
        <Drawer.Screen 
          name="Login" 
          component={hoc(Login, {...props})} 
          />
        <Drawer.Screen 
          name="Order" 
          component={hoc(Order, {...props})} 
          />
      </Drawer.Navigator>
    </NavigationContainer>
  )
}

export default DrawerComponent
