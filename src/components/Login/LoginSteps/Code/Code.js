import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import * as types from '../../../../store/actionTypes'
import {
  Keyboard,
} from 'react-native'
import {
  View,
  Text,
  Button,
  LoaderScreen,
} from 'react-native-ui-lib'
import {TextInputMask} from 'react-native-masked-text'

import {
  appColors,
  styles,
} from '../../../../assets/styles'
import axios from '../../../../axios'

class Code extends PureComponent {
  state = {
    code: '',
    enableServerError: false,
    enableValidError: false,
    enableLoader: false,
    currentUserStatus: '',
  }

  validationCodeHandler = () => {
    this.setState({
      enableServerError: false,
      enableValidError: false,
      enableLoader: true,
    })

    const params = new FormData(),
          self = this

    params.append('code', this.state.code)
    params.append('cityID', this.props.user.cityID)
    params.append('userID', this.props.user.userID)
    params.append('phone', this.props.user.phoneNumber)
    
    axios().post('/login-validation', params)
    .then(function (response) {
      if( response.data && response.data.token ) {
        self.setState({
          enableLoader: false,
        })

        self.props.updateMainInfo({
          token: response.data.token,
          cityName: response.data.cityName,
          cityCoords: {
            latitude: response.data.latitude,
            longitude: response.data.longitude,
          },
        })

        self.props.goToNextStep()
      }
    })
    .catch(function (error) {
      if( error.response.status == 403 ) {
        self.setState({
          enableValidError: true,
          enableLoader: false,
        })
      } else {
        self.setState({
          enableServerError: true,
          enableLoader: false,
        })
      }
    })
  }

  render() {
    const isCodeValid = this.state.code.length === 4 /* && this.state.code == this.props.code */

    return (
      <View flex centerH bg-lightBackground padding-s5>
        { (this.state.enableLoader) ?
        <LoaderScreen
          color={appColors.primaryColor}
          backgroundColor={appColors.modalBackgroundColor}
          overlay
        />
        : null }
        <Text subheading center marginB-s5>На Ваш номер отправлена СМС с кодом. Введите его ниже в течении 60 секунд:</Text>
        <View>
          <TextInputMask
            ref={ref => this.phoneInput = ref}
            style={[styles.singleTextInput, {width: 200}]}
            type={'custom'}
            options={{mask: '9999'}}
            value={this.state.code}
            keyboardType="numeric"
            placeholder="9999"
            onChangeText={code => this.setState({code})} />

          { (this.state.enableServerError) ?
            <View style={{width: 200}} marginT-s4>
              <Text subtext center errorColor>Произошла ошибка на сервере.</Text>
              <Text subtext center errorColor>Поробуйте подключиться позже.</Text>
            </View>
          : null }

          { (this.state.enableValidError) ?
            <View style={{width: 200}} marginT-s4>
              <Text subtext center errorColor>Ошибка введения кода.</Text>
              <Text subtext center errorColor>Введен неверный пароль или истекло время для ввода.</Text>
            </View>
          : null }
          
          <Button 
            backgroundColor={appColors.primaryColor}
            color={appColors.textColorInverted}
            size='large'
            style={{
              minWidth: 150,
              marginTop: 20,
            }}
            labelStyle={{
              fontSize: 20,
              paddingLeft: 20,
              paddingRight: 20,
            }}
            onPress={() => {
              this.validationCodeHandler()
              Keyboard.dismiss()
            }}
            disabled={!isCodeValid}
            label="Отправить" />
        </View>
      </View>
    )
  }
}

const stateToProps = state => {
  return {
    // code: state.mainInfo.code,
    user: state.mainInfo.user,
  }
}

const dispToProps = dispatch => {
  return {
    updateMainInfo: payload => dispatch({type: types.UPDATE_MAIN_INFO, payload}),
  }
}

export default connect(stateToProps, dispToProps)(Code)