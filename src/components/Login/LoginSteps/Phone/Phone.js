import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import * as types from '../../../../store/actionTypes'
import {
  Keyboard,
} from 'react-native'
import {
  View,
  Text,
  Button,
  LoaderScreen,
} from 'react-native-ui-lib'
import {TextInputMask} from 'react-native-masked-text'

import {
  appColors,
  addTypography,
  styles,
} from '../../../../assets/styles'
import {makeClearPhone} from '../../../../helpers'
import axios from '../../../../axios'

class Phone extends PureComponent {
  state = {
    phone: '',
    phoneNumber: '',
    enableError: false,
    enableLoader: false,
    currentUserStatus: '',
  }

  phoneHandler = () => {
    this.setState({
      enableError: false,
      enableLoader: true,
    })

    const params = new FormData(),
          self = this

    params.append('phone', '+38' + this.state.phoneNumber)

    axios().post('/login', params)
    .then(function (response) {
      if( response.data && response.data.userStatus ) {
        self.setState({
          currentUserStatus: response.data.userStatus,
          enableLoader: false,
        })
        self.props.updateMainInfo({
          user: response.data.user,
          // code: response.data.code,
        })
        self.props.goToNextStep()
        // self.props.visibleComponentHandler('steps')
      }
    })
    .catch(function (error) {
      console.log(error);
      self.setState({
        enableError: true,
        enableLoader: false,
      })
    })
  }

  render() {
    const isPhoneValid = this.state.phoneNumber.length === 10

    return (
      <View flex centerH bg-lightBackground padding-s5>
        { (this.state.enableLoader) ?
        <LoaderScreen
          color={appColors.primaryColor}
          backgroundColor={appColors.modalBackgroundColor}
          overlay
        />
        : null }
        <Text subheading center marginB-s5>Введите ваш номер телефона:</Text>
        <View>
          <TextInputMask
            ref={ref => this.phoneInput = ref}
            style={[styles.singleTextInput, {width: 200}]}
            type={'custom'}
            options={{mask: '(099) 999 99 99'}}
            value={this.state.phone}
            keyboardType="numeric"
            placeholder="(099) 999 99 99"
            onChangeText={phone => {
              this.setState({ 
                phone,
                phoneNumber: makeClearPhone(phone),
              })
            }} />

          { (this.state.enableError) ?
            <View marginT-s4>
              <Text subtext center errorColor>Произошла ошибка на сервере.</Text>
              <Text subtext center errorColor>Поробуйте подключиться позже.</Text>
            </View>
          : null }
          
          <Button 
            backgroundColor={appColors.primaryColor}
            color={appColors.textColorInverted}
            size='large'
            style={{
              minWidth: 150,
              marginTop: 20,
            }}
            labelStyle={{
              fontSize: 20,
              paddingLeft: 20,
              paddingRight: 20,
            }}
            onPress={() => {
              this.phoneHandler()
              Keyboard.dismiss()
            }}
            disabled={!isPhoneValid}
            label="Отправить" />
        </View>
      </View>
    )
  }
}

const dispToProps = dispatch => {
  return {
    updateMainInfo: payload => dispatch({type: types.UPDATE_MAIN_INFO, payload}),
  }
}

export default connect(null, dispToProps)(Phone)