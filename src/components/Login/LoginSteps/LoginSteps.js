import React, {Component} from 'react'
import {connect} from 'react-redux'
import {
  StyleSheet, 
  ScrollView,
  BackHandler,
} from 'react-native'
import {
  View, 
  Button, 
  Wizard, 
  Text, 
} from 'react-native-ui-lib'
import AsyncStorage from '@react-native-community/async-storage'

import Phone from './Phone/Phone'
import Code from './Code/Code'
import BaseUserData from './BaseUserData/BaseUserData'
import { appColors } from '../../../../src/assets/styles'

class LoginSteps extends Component {
  state = {
    activeIndex: 0,
    completedStepIndex: undefined,
    customerName: undefined,
  }

  componentDidUpdate(prevProps, prevState) {
    if( this.state.activeIndex == 3 && this.state.activeIndex != prevProps.activeIndex ) {
      this.saveStorageData({
        token: this.props.token,
        phone: this.props.user.phoneNumber,
      })
    }
  }

  onActiveIndexChanged = activeIndex => {
    this.setState({activeIndex})
  }

  reset = () => {
    const {customerName} = this.state;

    this.setState({
      activeIndex: 0,
      completedStepIndex: undefined,
      customerName: undefined,
    })
  }

  saveStorageData = async (data) => {
    try {
      const newData = {
        ...this.props.storageData,
        ...data,
      }

      await AsyncStorage.setItem('@storageData', JSON.stringify(newData))
    } catch (e) {
      console.log(e);
    }
  }

  goToNextStep = () => {
    const {activeIndex: prevActiveIndex, completedStepIndex: prevCompletedStepIndex} = this.state;
    const reset = prevActiveIndex === 3;
    if (reset) {
      this.reset();
      return;
    }

    const activeIndex = prevActiveIndex + 1;
    let completedStepIndex = prevCompletedStepIndex;
    if (!prevCompletedStepIndex || prevCompletedStepIndex < prevActiveIndex) {
      completedStepIndex = prevActiveIndex;
    }

    if (activeIndex !== prevActiveIndex || completedStepIndex !== prevCompletedStepIndex) {
      this.setState({activeIndex, completedStepIndex});
    }
  };

  renderCurrentStep = () => {
    const {activeIndex} = this.state;

    switch (activeIndex) {
      case 0:
        return <Phone goToNextStep={() => this.goToNextStep()} />
      case 1:
        return <Code goToNextStep={() => this.goToNextStep()} />
      case 2:
        return <BaseUserData goToNextStep={() => this.goToNextStep()} />
      case 3:
        return (
          <View flex centerH bg-lightBackground padding-s5>
            <Text subheading center marginB-s5>Авторизация завершена успешно!!! Подвезти?</Text>
            <Button 
              backgroundColor={appColors.primaryColor}
              color={appColors.textColorInverted}
              size='large'
              style={{
                minWidth: 200,
                marginTop: 20,
              }}
              labelStyle={{
                fontSize: 20,
                paddingLeft: 20,
                paddingRight: 20,
              }}
              onPress={() => {/* this.phoneHandler() */}}
              label="Да" />
            <Button 
              backgroundColor={appColors.secondaryColor}
              color={appColors.textColorInverted}
              size='large'
              style={{
                minWidth: 200,
                marginTop: 20,
              }}
              labelStyle={{
                fontSize: 20,
                paddingLeft: 20,
                paddingRight: 20,
              }}
              onPress={() => BackHandler.exitApp()}
              label="Позже" />
          </View>
        )
    }
  }

  getStepState(index) {
    const {activeIndex, completedStepIndex} = this.state;
    let state = Wizard.States.DISABLED
    if (completedStepIndex > index - 1) {
      state = Wizard.States.COMPLETED
    } else if (activeIndex === index || completedStepIndex === index - 1) {
      state = Wizard.States.ENABLED
    }

    return state
  }

  render() {
    const {activeIndex} = this.state

    return (
      <View flex>
        <ScrollView keyboardShouldPersistTaps='always' contentContainerStyle={styles.scrollView}>
          <View style={styles.container}>
            <Wizard testID={'uilib.wizard'} activeIndex={activeIndex} onActiveIndexChanged={this.onActiveIndexChanged}>
              <Wizard.Step 
                circleSize={30} 
                enabled={false}
                state={this.getStepState(0)} 
                label={'Телефон'}/>
              <Wizard.Step 
                circleSize={30} 
                enabled={false}
                state={this.getStepState(1)} 
                label={'Код Валидации'}/>
              <Wizard.Step 
                circleSize={30} 
                enabled={false}
                state={this.getStepState(2)} 
                label={'Данные пользователя'}/>
              <Wizard.Step 
                circleSize={30} 
                enabled={false}
                state={this.getStepState(3)} 
                label={'Завершено!!!'}/>
            </Wizard>
            {this.renderCurrentStep()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const stateToProps = state => {
  return {
    storageData: state.storageData,
    token: state.mainInfo.token,
    user: state.mainInfo.user,
  }
}

export default connect(stateToProps)(LoginSteps)

const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  },
  container: {
    flex: 1
  },
  stepContainer: {
    flex: 1,
    justifyContent: 'space-between',
    margin: 20
  }
})