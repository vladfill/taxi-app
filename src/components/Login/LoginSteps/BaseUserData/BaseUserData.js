import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import * as types from '../../../../store/actionTypes'
import {
  TextInput,
  Keyboard,
  PermissionsAndroid,
  ToastAndroid,
  Platform,
} from 'react-native'
import {
  View,
  Text,
  Button,
  Picker,
  LoaderScreen,
} from 'react-native-ui-lib'
// import algoliasearch from 'algoliasearch'
import axiosLib from 'axios'
import Geolocation from 'react-native-geolocation-service'
import AsyncStorage from '@react-native-community/async-storage'

import {
  appColors,
  styles,
} from '../../../../assets/styles'
import axios from '../../../../axios'

class BaseUserData extends PureComponent {
  state = {
    name: this.props.user.firstName,
    cityName: this.props.cityName ? {value: this.props.cityName, label: this.props.cityName}: '',
    cityLoc: {},
    citiesList: [],
    currentCoords: {},
    enableServerError: false,
    enableLoader: false,
  }

  hasLocationPermission = async () => {
    if (Platform.OS === 'ios') {
      const hasPermission = await this.hasLocationPermissionIOS();
      return hasPermission
    }

    if (Platform.OS === 'android' && Platform.Version < 23) {
      return true
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    )

    if (hasPermission) {
      return true
    }

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
      return true
    }

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show(
        'Location permission denied by user.',
        ToastAndroid.LONG,
      )
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show(
        'Location permission revoked by user.',
        ToastAndroid.LONG,
      )
    }

    return false
  }

  hasLocationPermissionIOS = async () => {
    const openSetting = () => {
      Linking.openSettings().catch(() => {
        Alert.alert('Unable to open settings');
      })
    }
    const status = await Geolocation.requestAuthorization('whenInUse');

    if (status === 'granted') {
      return true
    }

    if (status === 'denied') {
      Alert.alert('Location permission denied')
    }

    if (status === 'disabled') {
      Alert.alert(
        `Turn on Location Services to allow "${appConfig.displayName}" to determine your location.`,
        '',
        [
          { text: 'Go to Settings', onPress: openSetting },
          { text: "Don't Use Location", onPress: () => {} },
        ],
      )
    }

    return false;
  }

  componentDidMount() {
    this.getLocation()
  }

  getLocation = async () => {
    const hasLocationPermission = await this.hasLocationPermission();

    if (!hasLocationPermission) {
      return
    }

    Geolocation.getCurrentPosition(info => {
      const {latitude, longitude} = info.coords
      console.log(info);
      
      this.setState({currentCoords: {latitude, longitude}})
      this.props.updateMainInfo({currentCoords: {latitude, longitude}})
    })
  }

  updateUser = () => {
    this.setState({
      enableServerError: false,
      enableLoader: true,
    })

    const params = new FormData()

    params.append('firstName', this.state.name)
    params.append('city', this.state.cityName.value)
    params.append('latitude', this.state.cityLoc.latitude)
    params.append('longitude', this.state.cityLoc.longitude)

    axios(this.props.token).put('/users/' + this.props.user.userID, params)
    .then( (response) => {
      if( response.status == 204 ) {
        this.props.goToNextStep()
      }
    })
    .catch( (error) => {
      console.log(error.response);

      self.setState({
        enableServerError: true,
        enableLoader: false,
      })
    })
  }

  searchCity = (search) => {
    const headers = new Headers()
    const optionData = {
      query: search,
      type: 'city',
      language: ['ru', 'ua', 'en'],
      countries: 'ua',
    }

    if(this.state.currentCoords.latitude && this.state.currentCoords.longitude) {
      optionData.aroundLatLng = this.state.currentCoords.latitude + ', ' + this.state.currentCoords.longitude
      // optionData.aroundRadius= 10000
    }

    headers.append('Content-Type', 'application/json')
    headers.append('Accept', 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8')
    headers.append('X-Algolia-Application-Id', 'plMWLDLNKGI0')
    headers.append('X-Algolia-API-Key', '4253f7e55adf225fd0a209c8ab001e61')

    axiosLib.post('https://places-dsn.algolia.net/1/places/query', JSON.stringify(optionData), {headers})
    .then( (response) => {
      console.log(response.data);
      if(response.data.hits.length) {
        const citiesList = response.data.hits.filter(hit => hit._tags[0] === 'city' && hit.population > 10000)
        this.setState({citiesList})
      }
    })
    .catch((error) => {
      console.log(error);
    })
  }

  render() {
    const isButtonEnabled = this.state.name.length > 1 && Object.keys(this.state.cityName).length,
          isUserCreatedBefore = this.props.user.firstName && this.props.cityName

    return (
      <View flex centerH bg-lightBackground padding-s5>
        { (this.state.enableLoader) ?
        <LoaderScreen
          color={appColors.primaryColor}
          backgroundColor={appColors.modalBackgroundColor}
          overlay
        />
        : null }
        <Text subheading center marginB-s5>Номер телефона успешно идентифицинован! Введите Ваше имя и город в котором проживаете.</Text>
        <View>
          <TextInput
            ref={ref => this.phoneInput = ref}
            style={[styles.singleTextInput, {width: 200}]}
            value={this.state.name}
            placeholder="Ваше имя"
            onChangeText={name => this.setState({name})} />

          { (this.state.enableServerError) ?
            <View style={{width: 200}} marginT-s4>
              <Text subtext center errorColor>Произошла ошибка на сервере.</Text>
              <Text subtext center errorColor>Поробуйте подключиться позже.</Text>
            </View>
          : null }

          <Picker
            placeholder="Город"
            //floatingPlaceholder
            hideUnderline
            value={this.state.cityName}
            enableModalBlur={false}
            onChange={city => {
              const currentCity = this.state.citiesList.find(cityItem => cityItem.locale_names[0] === city.value)
              
              this.setState({
                cityName: city,
                cityLoc: {
                  latitude: currentCity._geoloc.lat,
                  longitude: currentCity._geoloc.lng,
                }
              })
            }}
            topBarProps={{title: 'Список городов'}}
            style={[styles.singleTextInput, {marginTop: 15,}]}
            //searchStyle={{color: Colors.blue30, placeholderTextColor: Colors.dark50}}
            showSearch
            searchPlaceholder={'Выберите Город'}
            onSearchChange={value => this.searchCity(value)}
            keyboardShouldPersistTaps='always'
          >
            {this.state.citiesList.map(city => { console.log(city);
            
              return <Picker.Item 
                key={city.objectID} 
                label={city.locale_names[0]}
                value={city.locale_names[0]}
                />
              }
            )}
          </Picker>
          
          <Button 
            backgroundColor={appColors.primaryColor}
            color={appColors.textColorInverted}
            size='large'
            style={{
              minWidth: 150,
              marginTop: 20,
            }}
            labelStyle={{
              fontSize: 20,
              paddingLeft: 20,
              paddingRight: 20,
            }}
            onPress={() => {
              this.updateUser()
              Keyboard.dismiss()
            }}
            disabled={!isButtonEnabled}
            label={isUserCreatedBefore ? "Обновить" : "Отправить"} />

            {
              (isUserCreatedBefore) ?
              <Button 
                backgroundColor={appColors.secondaryColor}
                color={appColors.textColorInverted}
                size='large'
                style={{
                  minWidth: 150,
                  marginTop: 20,
                }}
                labelStyle={{
                  fontSize: 20,
                  paddingLeft: 20,
                  paddingRight: 20,
                }}
                onPress={() => {
                  this.props.goToNextStep()
                  Keyboard.dismiss()
                }}
                label="Пропустить" /> : null
            }
        </View>
      </View>
    )
  }
}

const stateToProps = state => {
  return {
    cityName: state.mainInfo.cityName,
    token: state.mainInfo.token,
    user: state.mainInfo.user,
  }
}

const dispToProps = dispatch => {
  return {
    updateMainInfo: payload => dispatch({type: types.UPDATE_MAIN_INFO, payload}),
  }
}

export default connect(stateToProps, dispToProps)(BaseUserData)