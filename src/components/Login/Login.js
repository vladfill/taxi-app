import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import * as types from '../../store/actionTypes'
import Overlay from './Overlay/Overlay'
import LoginSteps from './LoginSteps/LoginSteps'
import AsyncStorage from '@react-native-community/async-storage'

class Login extends PureComponent {
  state = {
    visibleComponent: 'steps',
    step: 0,
  }

  visibleComponentHandler = (visibleComponent, step=0) => {
    this.setState({visibleComponent, step})
  }

  componentDidMount() {
    this.getSavedData()
  }

  getSavedData = async () => {
    try {
      const storageData = await AsyncStorage.getItem('@storageData')

      if(storageData !== null) {
        const storageDataParsed = JSON.parse(storageData)

        if(storageDataParsed.token && storageDataParsed.phone) {
          this.setState({
            visibleComponent: 'overlay',
            storageData: storageDataParsed,
          })
          console.log(storageDataParsed);
          
          this.props.updateStorageData({...storageDataParsed})
        }
      }
    } catch(e) {
      console.log(e);
    }
  }

  render() {
    let component = null
    switch (this.state.visibleComponent) {
      case 'steps':
        component = <LoginSteps />
        break;

      default:
        component = <Overlay navigation={this.props.navigation} visibleComponentHandler={(visibleComponent) => this.visibleComponentHandler(visibleComponent)} />
        break;
    }

    return component
  }
}

const dispToProps = dispatch => {
  return {
    updateStorageData: payload => dispatch({type: types.UPDATE_STORAGE_DATA, payload}),
  }
}

export default connect(null, dispToProps)(Login)