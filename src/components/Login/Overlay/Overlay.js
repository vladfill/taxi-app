import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import * as types from '../../../store/actionTypes'
import {
  StyleSheet,
  Dimensions,
} from 'react-native'
import {
  View,
  Text,
  Image,
} from 'react-native-ui-lib'
import AsyncStorage from '@react-native-community/async-storage'

import axios from '../../../axios'

screen = Dimensions.get('window')

class Overlay extends PureComponent {
  updateToken = () => {
    const params = new FormData()

    params.append('phone', this.props.storageData.phone)
    params.append('token', this.props.storageData.token)
    
    axios(this.props.storageData.token).post('/token', params)
    .then( response => {
      const {cityCoords, cityName, token, user} = response.data

      if( response.data ) {
        this.props.updateMainInfo({cityCoords, cityName, token, user})
        this.props.updateStorageData({token})
        if(this.saveStorageData({token})) {
          this.props.navigation.navigate('Order')
        }
      }
    })
    .catch( error => {
      this.props.visibleComponentHandler('steps', 3)
    })
  }

  saveStorageData = async (data) => {
    try {
      const newData = {
        ...this.props.storageData,
        ...data,
      }

      await AsyncStorage.setItem('@storageData', JSON.stringify(newData))
      return true
    } catch (e) {
      console.log(e);
      return false
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if( this.props.storageData.token && prevProps.storageData.token != this.props.storageData.token ) {
      this.updateToken()
    }
  }

  render() {
    return (
      <View flex bg-lightBackground>
        <Image 
          style={{
            width: screen.width,
            height: screen.height,
          }}
          source={require('../../../assets/images/login.png')} />
        <Text style={styles.loaderText}>{"Загрузка..."}</Text>
      </View>
    )
  }
}

const stateToProps = state => {
  return {
    storageData: state.storageData,
  }
}

const dispToProps = dispatch => {
  return {
    updateMainInfo: payload => dispatch({type: types.UPDATE_MAIN_INFO, payload}),
    updateStorageData: payload => dispatch({type: types.UPDATE_MAIN_INFO, payload}),
  }
}

export default connect(stateToProps, dispToProps)(Overlay)

const styles = StyleSheet.create({
  loaderContainer: {
    flex: 1,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loaderText: {
    fontSize: 16,
    textAlign: 'center',
  },
})