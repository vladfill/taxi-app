import * as types from '../actionTypes'

const initialState = {
  token: '',
  code: '',
  cityName: '',
  cityCoords: {
    latitude: 0, 
    longitude: 0,
  },
  user: {},
  currentCoords: {
    latitude: 0, 
    longitude: 0,
  }
}

const mainInfo = (state=initialState, action) => {
  const {type, payload} = action

  switch (type) {
    case types.UPDATE_MAIN_INFO:
      return {
        ...state,
        ...JSON.parse(JSON.stringify(payload)),
      }
  
    default:
      return {...state}
  }
}

export default mainInfo