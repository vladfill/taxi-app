import * as types from '../actionTypes'

const initialState = {
  phone: '',
  token: '',
}

const storageData = (state=initialState, action) => {
  const {type, payload} = action

  switch (type) {
    case types.UPDATE_STORAGE_DATA:
      return {
        ...state,
        ...JSON.parse(JSON.stringify(payload)),
      }
  
    default:
      return {...state}
  }
}

export default storageData