import * as types from '../actionTypes'
import {checkIfObjectNotEmpty} from '../../helpers/helpers'
const initialState = {
  orderID: 0,
  price: '',
  message: '',
  childChair: false,
  moreThat4Seats: false,
  coordsList: [],
  distance: 0,
  replaceIndex: -1,
}

const orderData = (state=initialState, action) => {
  switch (action.type) {
    case types.SAVE_ORDER:
      return {
        ...state,
        ...action.payload,
      }
    case types.SET_DISTANCE:
      return {
        ...state,
        distance: action.payload,
      }
    case types.ADD_COORD_TO_LIST:
      // Add item to end of array
      let coordsList = [...state.coordsList]
      if( coordsList.length && !checkIfObjectNotEmpty(coordsList[coordsList.length-1]) ) {
        coordsList[coordsList.length-1] = action.payload
      } else {
        coordsList.push(action.payload)
      }
      return {
        ...state,
        coordsList,
      }
    case types.DELETE_COORD_FROM_LIST:
      // Delete item by index
      coordsList = [...state.coordsList]
      coordsList.splice(action.payload, 1)

      return {
        ...state,
        coordsList: coordsList,
      }
    case types.REPLACE_COORD_FROM_LIST:
      // Replace item by index
      coordsList = [...state.coordsList]
      if(action.payload.index >= 0) {
        coordsList[action.payload.index] = {...action.payload.coord}
      }

      return {
        ...state,
        coordsList: coordsList,
      }
    case types.ADD_REPLACE_INDEX:
      // Add coord index for replace
      return {
        ...state,
        replaceIndex: action.payload,
      }
    case types.UPDATE_ADITIONAL_FIELDS:
      // Add coord index for replace
      return {
        ...state,
        ...action.payload,
      }

    default:
      return state
  }
}

export default orderData