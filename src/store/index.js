import { combineReducers } from 'redux'
import mainInfo from './reducers/mainInfo'
import storageData from './reducers/storageData'
// import orderData from './reducers/orderData'

export default combineReducers({
  mainInfo,
  storageData,
  // orderData,
})