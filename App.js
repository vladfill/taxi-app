import React from 'react'
import {
  SafeAreaView,
  StatusBar,
  Dimensions,
} from 'react-native'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import {
  Colors, 
  Typography,
} from 'react-native-ui-lib'
import reducer from './src/store'
import './src/helpers/prototype'
import {
  appColors, 
  addTypography,
} from './src/assets/styles'

import Drawer from './src/components/Drawer/Drawer'

const store = createStore(reducer)

Colors.loadColors({...appColors})
Typography.loadTypographies({...addTypography})

const dimensions = Dimensions.get('window')

const App = () => {
  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <StatusBar hidden={true} backgroundColor={appColors.lightBackground} barStyle="dark-content" />
        <Provider store={store}>
          <Drawer />
        </Provider>
      </SafeAreaView>
    </>
  );
};

export default App;
